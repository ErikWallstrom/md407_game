/*
	Code assumes the display is connected to port E
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "defines.h"
#include "delays.c"

#define B_E      0x40 // Enable
#define B_RST    0x20 // Reset
#define B_CS2    0x10 // Controller Select2
#define B_CS1    8    // Controller Select1
#define B_SELECT 4    // 0 Graphics, 1 ASCII
#define B_RW     2    // 0 Write, 1 Read
#define B_RS     1    // 0 Command, 1 Data

#define LCD_ON          0x3F // Display on
#define LCD_OFF         0x3E // Display off
#define LCD_SET_ADD     0x40 // Set horizontalcoordinate
#define LCD_SET_PAGE    0xB8 // Set verticalcoordinate
#define LCD_DISP_START  0xC0 // Start address
#define LCD_BUSY 		0x80 // Read busystatus

#define GRAPHIC_WIDTH 128
#define GRAPHIC_HEIGHT 64
#define GRAPHIC_BACK_BUFFER_SIZE 1024

static uint8_t graphic_back_buffer[GRAPHIC_BACK_BUFFER_SIZE];

static void 
graphic_clear_back_buffer(void)
{
	for(uint16_t i = 0; i < GRAPHIC_BACK_BUFFER_SIZE; i++)
	{
		graphic_back_buffer[i] = 0;
	}
}

static void 
graphic_ctrl_bit_set(uint8_t x)
{
	uint8_t c = gpio_e->odr_low;
	c |= x;
	gpio_e->odr_low = c;
}

static void 
graphic_ctrl_bit_clear(uint8_t x)
{
	uint8_t c = gpio_e->odr_low;
	c &= ~x;
	gpio_e->odr_low = c;
}

static void 
graphic_select_controller(uint8_t controller)
{
	switch(controller)
	{
	case 0:
		graphic_ctrl_bit_clear(B_CS1 | B_CS2);
		break;

	case B_CS1:
		graphic_ctrl_bit_set(B_CS1);
		graphic_ctrl_bit_clear(B_CS2);
		break;
	
	case B_CS2: 
		graphic_ctrl_bit_set(B_CS2);
		graphic_ctrl_bit_clear(B_CS1);
		break;
	
	case B_CS1|B_CS2:
		graphic_ctrl_bit_set(B_CS1 | B_CS2);
		break;
	}
}

static void 
graphic_wait_ready(void)
{
	uint8_t c;
	graphic_ctrl_bit_clear(B_E);
	gpio_e->moder = 0x00005555; // 15-8 inputs, 7-0 outputs
	graphic_ctrl_bit_clear(B_RS);
	graphic_ctrl_bit_set(B_RW);
	delay_500ns();
	while(1)
	{
		graphic_ctrl_bit_set(B_E);
		delay_500ns();
		c = gpio_e->idr_high & LCD_BUSY;
		graphic_ctrl_bit_clear(B_E);
		delay_500ns();
		if(c == 0) break;
	}

	gpio_e->moder =0x55555555; // 15-0 outputs
}


static void 
graphic_write(uint8_t value, uint8_t controller)
{
	gpio_e->odr_high = value;
	graphic_select_controller(controller);
	delay_500ns();
	graphic_ctrl_bit_set(B_E);
	delay_500ns();
	graphic_ctrl_bit_clear(B_E);

	if(controller & B_CS1)
	{
		graphic_select_controller(B_CS1);
		graphic_wait_ready();
	}

	if(controller & B_CS2)
	{
		graphic_select_controller(B_CS2);
		graphic_wait_ready();
	}

	gpio_e->odr_high = 0;
	graphic_ctrl_bit_set(B_E);
	graphic_select_controller(0);
}

static void 
graphic_write_command(uint8_t command, uint8_t controller)
{
	graphic_ctrl_bit_clear(B_E);
	graphic_select_controller(controller);
	graphic_ctrl_bit_clear(B_RS | B_RW);
	graphic_write(command, controller);
}

static void 
graphic_write_data(uint8_t data, uint8_t controller)
{
	graphic_ctrl_bit_clear(B_E);
	graphic_select_controller(controller);
	graphic_ctrl_bit_set(B_RS);
	graphic_ctrl_bit_clear(B_RW);
	graphic_write(data, controller);
}

static void 
graphic_clear_screen(void)
{
	for(uint8_t page = 0; page < 8; page++)
	{
		graphic_write_command(LCD_SET_PAGE | page, B_CS1 | B_CS2);
		graphic_write_command(LCD_SET_ADD  | 0   , B_CS1 | B_CS2);
		for(uint8_t add = 0; add < 64; add++) 
		{
			graphic_write_data(0, B_CS1 | B_CS2);
		}
	}
}

static void 
graphic_init(void)
{
	gpio_e->moder = 0x55555555;

	graphic_ctrl_bit_set(B_E);
	delay_micro(10);
	graphic_ctrl_bit_clear(B_CS1 | B_CS2 | B_RST | B_E);
	delay_milli(30);
	graphic_ctrl_bit_set(B_RST);
	delay_milli(100);
	graphic_write_command(LCD_OFF, B_CS1 | B_CS2);
	graphic_write_command(LCD_ON, B_CS1 | B_CS2);
	graphic_write_command(LCD_DISP_START, B_CS1 | B_CS2);
	graphic_write_command(LCD_SET_ADD, B_CS1 | B_CS2);
	graphic_write_command(LCD_SET_PAGE, B_CS1 | B_CS2);
	graphic_select_controller(0);

#ifndef SIMULATOR
	graphic_clear_screen();
#endif
}

static uint8_t 
graphic_read(uint8_t controller)
{
	uint8_t c;
	graphic_ctrl_bit_clear(B_E);
	gpio_e->moder = 0x00005555; // 15-8 inputs, 7-0 outputs
	graphic_ctrl_bit_set(B_RS | B_RW);
	graphic_select_controller(controller);
	delay_500ns();
	graphic_ctrl_bit_set(B_E);
	delay_500ns();
	c = gpio_e->idr_high;
	graphic_ctrl_bit_clear(B_E);
	gpio_e->moder = 0x55555555; // 15-0 outputs
	if(controller & B_CS1)
	{
		graphic_select_controller(B_CS1);
		graphic_wait_ready();
	}

	if(controller & B_CS2)
	{
		graphic_select_controller(B_CS2);
		graphic_wait_ready();
	}

	return c;
}

static uint8_t 
graphic_read_data(uint8_t controller)
{
	graphic_read(controller);
	return graphic_read(controller);
}

static void 
graphic_draw_pixel(int16_t x, int16_t y, int set)
{
	if((x < 1) || (y < 1) || (x > GRAPHIC_WIDTH) || (y > GRAPHIC_HEIGHT))
	{
		return;
	}
	
	uint8_t mask = 1 << ((y - 1) % 8);

#ifndef SIMULATOR
	int index = 0;
	if(x > 64)
	{
		x -= 65;
		index = 512;
	}

	index += x + ((y - 1) / 8) * 64;
	graphic_back_buffer[index] |= mask;
#else
	int index = (y - 1) / 8;
	uint8_t controller;
	if(set == 0)
	{
		mask = ~mask;
	}

	if(x > 64)
	{
		controller = B_CS2;
		x = x -65;
	}
	else
	{
		controller = B_CS1;
		x = x - 1;
	}

	graphic_write_command(LCD_SET_ADD  | x,     controller);
	graphic_write_command(LCD_SET_PAGE | index, controller);

	uint8_t c = graphic_read_data(controller);
	graphic_write_command(LCD_SET_ADD  | x,     controller);
	if(set)
	{
		mask = mask | c;
	}
	else
	{
		mask = mask & c;
	}

	graphic_write_data(mask, controller);
#endif
}

static void 
graphic_draw_screen(void)
{
	uint32_t k = 0;
	for(uint8_t c = 0; c < 2; c++)
	{
		uint8_t controller = (c == 0) ? B_CS1 : B_CS2;
		for(uint8_t i = 0; i < 8; i++)
		{
			graphic_write_command(LCD_SET_PAGE | i, controller);
			graphic_write_command(LCD_SET_ADD  | 0, controller);
			for(uint8_t j = 0; j <= 63; j++)
			{
				k++;
				graphic_write_data(graphic_back_buffer[k], controller);
			}
		}
	}
}

#endif
