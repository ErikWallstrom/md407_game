#ifndef MARIO_H
#define MARIO_H

#include "goombas.c"
#include "world.c"
#include "object.c"
#include "keypad.c"
#include "res/mario1.xbm"
#include "res/mario2.xbm"

#define MARIO_JUMP_AMOUNT 21
#define MARIO_GRAVITY 4
#define MARIO_WIDTH  16
#define MARIO_HEIGHT 16

#ifdef SIMULATOR
#define MARIO_SPEED 6
#else
#define MARIO_SPEED 2
#endif

extern struct Goomba goomba;

static const struct Sprite 
mario_1 = {
	.w    = mario1_width,
	.h    = mario1_height,
	.data = mario1_bits
};

static const struct Sprite 
mario_2 = {
	.w    = mario2_width,
	.h    = mario2_height,
	.data = mario2_bits
};

struct Mario
{
	struct Object o;
	int standing_on_ground;
};

static void
mario_init(struct Mario* m)
{
	m->standing_on_ground = 0;
	object_init(&m->o, &mario_2, 0, 0);
}

static void 
mario_update(struct Mario* m)
{
	int moved_x = 0;
	switch(keypad_get_input())
	{
	case 0xE: 
		m->o.vel_x = -MARIO_SPEED; 
		moved_x = 1;
		break;

	case 0xF: 
		m->o.vel_x =  MARIO_SPEED; 
		moved_x = 1;
		break;

	case 0x8: 
		if(m->standing_on_ground)
		{
			m->o.vel_y = -MARIO_JUMP_AMOUNT; 
			m->standing_on_ground = 0;
		}
		break;

	default: 
		break;
	}

	if(!moved_x)
	{
		m->o.vel_x = 0;
	}

	if(!m->standing_on_ground)
	{
		m->o.vel_y += MARIO_GRAVITY;
	}

	//Collisions
	if(!goomba.dead)
	{
		if(    goomba.o.y >= m->o.y + m->o.s->h 
			&& goomba.o.y <= m->o.y + m->o.s->h + m->o.vel_y)
		{
			if(    (m->o.x + m->o.vel_x >= goomba.o.x
					&& m->o.x + m->o.vel_x < goomba.o.x + goomba.o.s->w
				|| (m->o.x + m->o.vel_x + m->o.s->w > goomba.o.x
					&& m->o.x + m->o.vel_x + m->o.s->w < goomba.o.x + goomba.o.s->w)))
			{
				m->o.vel_y = -MARIO_JUMP_AMOUNT; 
				m->standing_on_ground = 0;
				goomba.dead = 1;
			}
		}
		
		if(    (m->o.y + m->o.vel_y >= goomba.o.y
			 && m->o.y + m->o.vel_y < goomba.o.y + goomba.o.s->h)
			|| (m->o.y + m->o.vel_y + m->o.s->h > goomba.o.y
			 && m->o.y + m->o.vel_y + m->o.s->h < goomba.o.s->h))
		{
			if(     (goomba.o.x >= m->o.x + m->o.s->w 
				&&  goomba.o.x <= m->o.x + m->o.s->w + m->o.vel_x)
				|| (goomba.o.x + goomba.o.s->w <= m->o.x
				&&  goomba.o.x + goomba.o.s->w >= m->o.x + m->o.vel_x))
			{
				delay_milli(500);
				camera_x = 0;
				mario_init(m);
				return;
			}
		}
	}
	
	int collision = 0;
	for(uint8_t i = 0; i < WORLD_TILES_Y; i++)
	{
		if(    i * TILE_HEIGHT >= m->o.y + m->o.s->h 
			&& i * TILE_HEIGHT <= m->o.y + m->o.s->h + m->o.vel_y)
		{
			for(uint8_t j = 0; j < WORLD_TILES_X; j++)
			{
				if(world[i][j])
				{
					if(    (m->o.x + m->o.vel_x >= j * TILE_WIDTH
						 && m->o.x + m->o.vel_x < (j + 1) * TILE_WIDTH)
						|| (m->o.x + m->o.vel_x + m->o.s->w > j * TILE_WIDTH
						 && m->o.x + m->o.vel_x + m->o.s->w < (j + 1) * TILE_WIDTH))
					{
						m->o.vel_y = i * TILE_HEIGHT - m->o.y - m->o.s->h;
						m->standing_on_ground = 1;
						collision = 1;
						break;
					}
				}
			}
		}
		else if((i + 1) * TILE_HEIGHT - 1 <= m->o.y
			&&  (i + 1) * TILE_HEIGHT - 1 >= m->o.y + m->o.vel_y)
		{
			for(uint8_t j = 0; j < WORLD_TILES_X; j++)
			{
				if(world[i][j])
				{
					if(    (m->o.x + m->o.vel_x >= j * TILE_WIDTH
						 && m->o.x + m->o.vel_x < (j + 1) * TILE_WIDTH)
						|| (m->o.x + m->o.vel_x + m->o.s->w > j * TILE_WIDTH
						 && m->o.x + m->o.vel_x + m->o.s->w < (j + 1) * TILE_WIDTH))
					{
						if(world[i][j] == 2)
						{
							world[i][j] = 3;
						}
						
						m->o.vel_y = 0;
						m->o.y = (i + 1) * TILE_HEIGHT;
						break;
					}
				}
			}
		}
	}

	if(!collision)
	{
		m->standing_on_ground = 0;
	}

	for(uint8_t i = 0; i < WORLD_TILES_Y; i++)
	{
		if(    (m->o.y + m->o.vel_y >= i * TILE_HEIGHT
			 && m->o.y + m->o.vel_y < (i + 1) * TILE_HEIGHT)
			|| (m->o.y + m->o.vel_y + m->o.s->h > i * TILE_HEIGHT
			 && m->o.y + m->o.vel_y + m->o.s->h < (i + 1) * TILE_HEIGHT))
		{
			for(uint8_t j = 0; j < WORLD_TILES_X; j++)
			{
				if(world[i][j])
				{
					if(     j * TILE_WIDTH >= m->o.x + m->o.s->w 
						&&  j * TILE_WIDTH <= m->o.x + m->o.s->w + m->o.vel_x)
					{
						m->o.vel_x = j * TILE_WIDTH - m->o.x - m->o.s->w;
						break;
					}
					else if((j + 1) * TILE_WIDTH <= m->o.x
						&&  (j + 1) * TILE_WIDTH >= m->o.x + m->o.vel_x)
					{
						m->o.vel_x = (j + 1) * TILE_WIDTH - m->o.x;
						break;
					}
				}
			}
		}
	}

	if(m->o.x + m->o.vel_x < 0)
	{
		m->o.vel_x = -m->o.x;
	}
	/*else if(m->o.x + m->o.vel_x + m->o.s->w > GRAPHIC_WIDTH - 1)
	{
		m->o.vel_x = GRAPHIC_WIDTH - m->o.x - m->o.s->w - 1;
	}*/

 	//Gameover
	if(m->o.y > GRAPHIC_HEIGHT)
	{
		delay_milli(500);
		camera_x = 0;
		mario_init(m);
		return;
	}

	//Win
	if(m->o.x > (WORLD_TILES_X - 1) * TILE_WIDTH - 8)
	{
		gpio_d->odr_low = 0xFF;
	}

	//Animation
	if(m->o.vel_x > 0)
	{
		m->o.s = &mario_2;
	}
	else if(m->o.vel_x < 0)
	{
		m->o.s = &mario_1;
	}

	if(m->o.x > GRAPHIC_WIDTH / 2 - MARIO_WIDTH / 2 && m->o.x < WORLD_TILES_X * TILE_WIDTH - GRAPHIC_WIDTH / 2 - MARIO_WIDTH / 2)
	{
		camera_x -= m->o.vel_x;
	}
	
	object_update(&m->o);
}

static void
mario_draw(struct Mario* m)
{
#ifdef SIMULATOR
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1,     1);
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1 + 1, 1);
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1 - 1, 1);
//	graphic_draw_pixel(m->o.x + 1 + 1, m->o.y + 1,     1);
//	graphic_draw_pixel(m->o.x + 1 - 1, m->o.y + 1,     1);
#else
	object_draw(&m->o);
#endif
}

static void
mario_clear(struct Mario* m)
{
#ifdef SIMULATOR
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1,     0);
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1 + 1, 0);
	graphic_draw_pixel(m->o.x + 1,     m->o.y + 1 - 1, 0);
//	graphic_draw_pixel(m->o.x + 1 + 1, m->o.y + 1,     0);
//	graphic_draw_pixel(m->o.x + 1 - 1, m->o.y + 1,     0);
#else
	object_clear(&m->o);
#endif
}

#endif
