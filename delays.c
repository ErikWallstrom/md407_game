#ifndef DELAYS_H
#define DELAYS_H

#define STK_CTRL ((volatile uint32_t *) 0xE000E010)
#define STK_LOAD ((volatile uint32_t *) 0xE000E014)
#define STK_VAL  ((volatile uint32_t *) 0xE000E018)

#include "defines.h"

static void 
delay_250ns(void)
{
	*STK_CTRL = 0; // SystemCoreClock = 168000000
	*STK_LOAD = 168 / 4 - 1;
	*STK_VAL  = 0;
	*STK_CTRL = 5;

	while((*STK_CTRL & 0x10000) == 0);
	*STK_CTRL = 0;
}

static void 
delay_500ns(void)
{
	delay_250ns();
	delay_250ns();
}

static void 
delay_micro(uint32_t t)
{
	while(t > 0)
	{
		delay_500ns();
		delay_500ns();
		t--;
	}
}

static void 
delay_milli(uint32_t t)
{
#ifdef SIMULATOR
	t = 1;
#endif

	while(t > 0)
	{
		delay_micro(1000);
		t--;
	}
}

#endif
