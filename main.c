#include "mario.c"
#include "world.c"
#include "goombas.c"

struct Goomba goomba;

__attribute__((naked)) __attribute__((section (".start_section"))) 
void startup (void)
{
	__asm volatile(
		" LDR R0,=0x2001C000\n" //set stack
		" MOV SP,R0\n"
		" BL main\n"    		// call main
		"_exit: B .\n"    		// never return
	);
}

int main(void)
{
	graphic_init();
	keypad_init();

	goombas_init(&goomba);

	struct Mario mario;
	mario_init(&mario);

	gpio_d->moder  |= 0x00005555;
	gpio_d->odr_low = 0x80;

	while(1)
	{
		graphic_clear_back_buffer();
		world_draw();

		//Graphics draw code here
		mario_update(&mario);
		mario_draw(&mario);
		
		goomba_update(&goomba);
		goomba_draw(&goomba);

		delay_milli(1000 / TARGET_FPS);
		graphic_draw_screen();
	}
}

