#ifndef SPRITE_H
#define SPRITE_H

#include "defines.h"
#include "graphics.c"

struct Sprite
{
	uint8_t w;
	uint8_t h;
	uint8_t* data;
};

int camera_x;

static void sprite_draw(const struct Sprite* s, int16_t x, int16_t y, int set)
{
	uint8_t width_in_bytes;
	if(s->w % 8 == 0)
	{
		width_in_bytes = s->w / 8;
	}
	else
	{
		width_in_bytes = s->w / 8 + 1;
	}

	for(uint8_t i = 0; i < s->h; i++)
	{
		for(uint8_t j = 0; j < width_in_bytes; j++)
		{
			uint8_t byte = s->data[i * width_in_bytes + j];
			for(uint8_t k = 0; k < 8; k++)
			{
				if(byte & (1 << k))
				{
					graphic_draw_pixel(8 * j + k + x + 1 + camera_x, i + y + 1, set);
				}
			}
		}
	}
}

#endif
