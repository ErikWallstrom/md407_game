#ifndef DEFINES_H
#define DEFINES_H

//#define SIMULATOR
#define TARGET_FPS 60

typedef unsigned char  uint8_t;
typedef short   	   int16_t;
typedef unsigned short uint16_t;
typedef unsigned int   uint32_t;

struct GPIO
{
	uint32_t moder;
	uint16_t otyper;
	uint32_t ospeedr;
	uint32_t pupdr;
	uint8_t idr_low;
	uint8_t idr_high;
	_Alignas(uint32_t) 
	uint8_t odr_low;
	uint8_t odr_high;
};

static struct GPIO* gpio_e = (struct GPIO*) 0x40021000;
static struct GPIO* gpio_d = (struct GPIO*) 0x40020C00;

#endif
