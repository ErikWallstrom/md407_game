#ifndef GOOMBAS_H
#define GOOMBAS_H

#include "object.c"
#include "world.c"
#include "res/goomba1.xbm"
#include "res/goobma2.xbm"

#define GOOMBA_SPEED 2

static const struct Sprite 
goomba_1 = {
	.w    = goomba1_width,
	.h    = goomba1_height,
	.data = goomba1_bits
};

static const struct Sprite 
goomba_2 = {
	.w    = goobma2_width,
	.h    = goobma2_height,
	.data = goobma2_bits
};

struct Goomba
{
	struct Object o;
	int dead;
};

static void
goombas_init(struct Goomba* g)
{
	object_init(&g->o, &goomba_2, 20 * TILE_WIDTH, GRAPHIC_HEIGHT - TILE_HEIGHT * 2);
	g->dead = 0;
	g->o.vel_x = GOOMBA_SPEED;

}

static void 
goomba_update(struct Goomba* g)
{
	if(g->o.vel_x > 0)
	{
		g->o.s = &goomba_2;
	}
	else
	{
		g->o.s = &goomba_1;
	}
	
	if(!world[3][(g->o.x + goomba1_width / 2 + g->o.vel_x) / TILE_WIDTH])
	{
		g->o.vel_x = -g->o.vel_x;
	}
	
	object_update(&g->o);
}

static void
goomba_draw(struct Goomba* g)
{
	if(!g->dead)
	{
		object_draw(&g->o);
	}
}


#endif
