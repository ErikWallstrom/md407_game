#ifndef OBJECT_H
#define OBJECT_H

#include "sprite.c"

struct Object
{
	const struct Sprite* s;
	int16_t x;
	int16_t y;
	int16_t vel_x;
	int16_t vel_y;
};

static void 
object_init(struct Object* o, const struct Sprite* s, uint16_t x, uint16_t y)
{
	o->s = s;
	o->x = x;
	o->y = y;
	o->vel_x = 0;
	o->vel_y = 0;
}

static void 
object_update(struct Object* o)
{
	o->x += o->vel_x;
	o->y += o->vel_y;
}

static void 
object_draw(struct Object* o)
{
	sprite_draw(o->s, o->x, o->y, 1);
}

static void 
object_clear(struct Object* o)
{
	sprite_draw(o->s, o->x, o->y, 0);
}

#endif
