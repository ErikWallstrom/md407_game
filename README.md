# MD407_Game

Super Mario like game running on MD407 hardware

[Video of game running](https://www.youtube.com/watch?v=Hdk1oGOiOd0)

## Instructions
	
* `git clone https://gitlab.com/ErikWallstrom/md407_game.git`
* Create new project in CodeLite from user template md407-startup
* Delete `startup.c` from the project src folder. 
* Right click the src folder in CodeLite and press "add an existing file", and add `main.c` only.
* Build by pressing F7 in CodeLite
