/*
 	Code assumes the keypad is connected to port D high
 */

#ifndef KEYPAD_H
#define KEYPAD_H

#include "defines.h"

#define KEYPAD_NO_INPUT 0xFF

static void 
keypad_init(void)
{
	gpio_d->moder  |= 0x55000000;
	gpio_d->otyper |= 0x0F00;
	gpio_d->pupdr  |= 0x00AA0000;
}

static void 
keypad_activate_row(uint8_t row)
{
	gpio_d->odr_high = (1 << 3) << row;
}

static uint8_t 
keypad_read_column(void)
{
	uint8_t c = gpio_d->idr_high;
	for(uint8_t i = 0; i < 4; i++)
	{
		if(c & (1 << i))
		{
			return i + 1;
		}
	}

	return 0;
}

static uint8_t 
keypad_get_input(void)
{
	static uint8_t 
	keyvalues[4][4] = {
		{ 1, 2, 3,  10},
		{ 4, 5, 6,  11},
		{ 7, 8, 9,  12},
		{14, 0, 15, 13}
	};

	for(uint8_t row = 1; row <= 4; row++)
	{
		keypad_activate_row(row);
		uint8_t column = keypad_read_column();
		if(column)
		{
			return keyvalues[row - 1][column - 1];
		}
	}

	return KEYPAD_NO_INPUT;
}

#endif
