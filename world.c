#ifndef WORLD_H
#define WORLD_H

#include "res/tile.xbm"
#include "res/ground.xbm"
#include "res/block.xbm"
#include "res/flag.xbm"
#include "res/question.xbm"
#include "sprite.c"

#define WORLD_TILES_X 31
#define WORLD_TILES_Y 4
#define TILE_WIDTH 16
#define TILE_HEIGHT 16

static const struct Sprite
tiles[4] = {
	{.w = tile_width,     .h = tile_height,     .data = tile_bits    },
	{.w = block_width,    .h = block_height,    .data = block_bits   },
	{.w = ground_width,   .h = ground_height,   .data = ground_bits  },
	{.w = question_width, .h = question_height, .data = question_bits}
};

static const struct Sprite
flag = {
	.w 	  = flag_width,
	.h 	  = flag_height,
	.data = flag_bits
};

static uint8_t 
world[WORLD_TILES_Y][WORLD_TILES_X] = {
	{0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 2, 4, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0},
	{1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1}
};

static void world_draw(void)
{
	for(uint8_t i = 0; i < WORLD_TILES_Y; i++)
	{
		for(uint8_t j = 0; j < WORLD_TILES_X; j++)
		{
			if(world[i][j])
			{
				sprite_draw(
					&tiles[world[i][j] - 1], 
					j * tile_width, 
					i * tile_height, 
					1
				);
			}
		}
	}
	
	sprite_draw(&flag, (WORLD_TILES_X - 1) * TILE_WIDTH - 8, 0, 1);
}

#endif
